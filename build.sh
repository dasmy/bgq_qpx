#!/bin/bash

mpixlf90_r -d -qsuffix=cpp=f90 -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict -qunroll=yes main.f90
