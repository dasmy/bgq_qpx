# BG/Q QPX

This repository collects my attempts of making use of automatic and manual vectorization/SIMDzation on IBM Blue Gene/Q systems, specifically
[JUQUEEN at Juelich Supercomputing Centre](http://www.fz-juelich.de/ias/jsc/juqueen).


**Table of contents**

[TOC]

## Initial code example
The initial code example is a snippet from the interaction kernel for direct 3D Coulomb interaction between two particles as it appears in
the [Pretty Efficient Parallel Coulomb solver (PEPC)](http://www.fz-juelich.de/ias/jsc/pepc) developed at JSC with some wrapping code for
calling the routine and gathering some runtime measurements:

    :::fortran
    module bla
      implicit none
      save
        integer, parameter :: NN = 500
        integer, parameter :: Niter = 100000
    !
        integer, parameter :: kfp = 8 ! numeric precision (kind value)
        integer, public, parameter :: kind_particle     = kind(1_8) ! ATTENTION, see above
        real(kfp), parameter :: one = 1._kfp
        real(kfp) :: eps = 0.1

          type t_particle_pack
             real*8, allocatable :: ex(:)
             real*8, allocatable :: ey(:)
             real*8, allocatable :: ez(:)
             real*8, allocatable :: pot(:)
             real*8, allocatable :: q(:)
          end type t_particle_pack

          type t_tree_node_interaction_data
            real*8 :: coc(3) ! centre of charge
            real*8 :: charge ! net charge sum
            real*8 :: abs_charge ! absolute charge sum
            real*8 :: dip(3) ! dipole moment
            real*8 :: quad(3) ! diagonal quadrupole moments
            real*8 :: xyquad ! other quadrupole moments
            real*8 :: yzquad
            real*8 :: zxquad
            real*8 :: bmax
          end type t_tree_node_interaction_data


        public calc_force_coulomb_3D_direct

      contains

        subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t, eps2)
          implicit none
    !
          real(kfp), intent(in) :: delta(:,:)
          real(kfp), intent(in) :: dist2(:)
          type(t_particle_pack), intent(inout) :: particle_pack
          type(t_tree_node_interaction_data), intent(in) :: t
          real(kfp), intent(in) :: eps2
    !
          real(kfp) :: rd,r,rd3charge
          integer(kind_particle) :: ip, np
    !
          np = size(dist2, kind = kind_particle)
    !
          do ip = 1, np
    !
            if (dist2(ip) > 0.0_kfp) then
              r = sqrt(dist2(ip) + eps2)
              rd = one/r
              rd3charge = t%charge*rd*rd*rd
    !
              particle_pack%pot(ip) = particle_pack%pot(ip) + t%charge*rd
              particle_pack%ex(ip) = particle_pack%ex(ip) + rd3charge*delta(ip,1)
              particle_pack%ey(ip) = particle_pack%ey(ip) + rd3charge*delta(ip,2)
              particle_pack%ez(ip) = particle_pack%ez(ip) + rd3charge*delta(ip,3)
            end if
          end do
        end subroutine calc_force_coulomb_3D_direct

    end module bla


    program main
      use bla
      implicit none
      include 'mpif.h'

      type(t_particle_pack) :: p
      real(kfp), allocatable :: delta(:,:)
      real(kfp), allocatable :: dist2(:)
      type(t_tree_node_interaction_data) :: node
      real*8 :: t1, t2
      integer :: i


      allocate(p%ex(NN), p%ey(NN), p%ez(NN), p%pot(NN), dist2(NN), delta(NN,1:3))
      dist2(:) = 1._kfp

      t1 = MPI_WTIME()
      do i=1,Niter
        call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
      end do
      t2 = MPI_WTIME()
      write(*,'("Total time: ", f12.8)') t2-t1


      deallocate(p%ex, p%ey, p%ez, p%pot)
    end program
                    
After downloading some  commit from this repository, there will always be some file called `build.sh` that can be called to compile the
example files.

All currently available code versions have already been uploaded, runtimes are shown in the respective `run.out` files as well as in the
commit messages. Some more comments on what has been done in the different commits will follow.

The total runtime for this version is **6.36 seconds**.




****************************************




## Automatic vectorization

The following notes strictly follow my experimental traces that are reflected by the [commits in this repository](/dasmy/bgq_qpx/commits/all). Thus, unless otherwise noted, the different changes listed here were not reverted before applying the
next one but were applied successively on top of each other.
Thus, a specific change that improved performance can be the combined effect of several previous changes.
Accordingly, this listing will not pinpoint specific optimization strategies, but hints to some pitfalls that will defenitively prevent
automatic vectorization.
Sorry for being slightly non-systematic in some places :-)

### Replacing conditional jumps with masks
Conditional jumps in loops to be vectorized are considered to be a no-go. Thus, I replaced them with a separate loop for producing a mask
that is used in the main loop to in/exclude terms in the global summation:

The relevant diff is available [here](/dasmy/bgq_qpx/commits/5ab3dd711b2d1be345dcd0804253791027553758#chg-main.f90).

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -46,21 +46,28 @@ module bla
     !
           real(kfp) :: rd,r,rd3charge
           integer(kind_particle) :: ip, np
    +      
    +      real(kfp) :: mask(size(dist2))
     !
           np = size(dist2, kind = kind_particle)
    +
    +      where (dist2 > 0.0_kfp)
    +        mask = 1._kfp
    +      elsewhere
    +        mask = 0._kfp
    +      end where
    +
     !
           do ip = 1, np
     !
    -        if (dist2(ip) > 0.0_kfp) then
               r = sqrt(dist2(ip) + eps2)
               rd = one/r
               rd3charge = t%charge*rd*rd*rd
     !
    -          particle_pack%pot(ip) = particle_pack%pot(ip) + t%charge*rd
    -          particle_pack%ex(ip) = particle_pack%ex(ip) + rd3charge*delta(ip,1)
    -          particle_pack%ey(ip) = particle_pack%ey(ip) + rd3charge*delta(ip,2)
    -          particle_pack%ez(ip) = particle_pack%ez(ip) + rd3charge*delta(ip,3)
    -        end if
    +          particle_pack%pot(ip) = particle_pack%pot(ip) + mask(ip)*t%charge*rd
    +          particle_pack%ex(ip)  = particle_pack%ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    +          particle_pack%ey(ip)  = particle_pack%ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    +          particle_pack%ez(ip)  = particle_pack%ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
           end do
         end subroutine calc_force_coulomb_3D_direct


This did not change anything for now.

The total runtime for this version is **6.42 seconds**.


### Using `qnostrict` compiler option
Somewhere I read that for autovectorization to take place, the compiler option `qnostrict` has to be supplied.

The relevant diff is available [here](/dasmy/bgq_qpx/commits/76b54a83d0c89fa397dec0b3a467ebfcf1a14955#chg-build.sh).

    :::diff
    --- a/build.sh
    +++ b/build.sh
    @@ -1,3 +1,3 @@
     #!/bin/bash

    -mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto main.f90
    +mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict main.f90

This indeed has an impact: the code is more than 2.5 times faster.
The [compiler optimization listing](/dasmy/bgq_qpx/src/76b54a83d0c89fa397dec0b3a467ebfcf1a14955/main.lst#cl-867) did not show many SIMDized loopes, though.

The total runtime for this version is **2.41 seconds**.


### Inserting alignment hints
Correct alignment of variables is necessary for the compiler to allow for using QPX instructions.
Using the vendor-specific function `alignx()` we can assert alignment to the compiler.
Well, how should I as a Fortran-programmer know anything about alignment? - There is no access to variable addresses etc.
The language is specifically designed to hide all technical internals from the programmer.
Now, why doesn't the compiler know about alignment by itself? - I do not have any idea.

The diff for adding alignment hints is available [here](/dasmy/bgq_qpx/commits/9b8f7f0f4ac99d4c71f295d4d97c812b311fecc9#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -56,8 +56,15 @@ module bla
           elsewhere
             mask = 0._kfp
           end where
    -
     !
    +      call alignx(32, particle_pack%pot)
    +      call alignx(32, particle_pack%ex)
    +      call alignx(32, particle_pack%ey)
    +      call alignx(32, particle_pack%ez)
    +      call alignx(32, dist2)
    +      call alignx(32, delta)
    +!
    +      !ibm* assert(nodeps,itercnt(NN))
           do ip = 1, np
     !
               r = sqrt(dist2(ip) + eps2)

Again: no impact, no additional SIMDization inside the [compiler optimization listing](/dasmy/bgq_qpx/src/9b8f7f0f4ac99d4c71f295d4d97c812b311fecc9/main.lst#cl-847).

The total runtime for this version is **2.40 seconds**.

### Static allocation, removal of alignment hints
Maybe, vectorization does not work for allocatable arrays for whichever reason - switching to statically allocated arrays.
Alignment hints are removed again to avoid biasing of the results.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/827f5128b0a547d71e3276afa9fa85bff089dcca#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -11,11 +11,11 @@ module bla
         real(kfp) :: eps = 0.1

           type t_particle_pack
    -         real*8, allocatable :: ex(:)
    -         real*8, allocatable :: ey(:)
    -         real*8, allocatable :: ez(:)
    -         real*8, allocatable :: pot(:)
    -         real*8, allocatable :: q(:)
    +         real*8 :: ex(NN)
    +         real*8 :: ey(NN)
    +         real*8 :: ez(NN)
    +         real*8 :: pot(NN)
    +         real*8 :: q(NN)
           end type t_particle_pack

           type t_tree_node_interaction_data
    @@ -56,15 +56,8 @@ module bla
           elsewhere
             mask = 0._kfp
           end where
    +
     !
    -      call alignx(32, particle_pack%pot)
    -      call alignx(32, particle_pack%ex)
    -      call alignx(32, particle_pack%ey)
    -      call alignx(32, particle_pack%ez)
    -      call alignx(32, dist2)
    -      call alignx(32, delta)
    -!
    -      !ibm* assert(nodeps,itercnt(NN))
           do ip = 1, np
     !
               r = sqrt(dist2(ip) + eps2)
    @@ -87,14 +80,12 @@ program main
       include 'mpif.h'

       type(t_particle_pack) :: p
    -  real(kfp), allocatable :: delta(:,:)
    -  real(kfp), allocatable :: dist2(:)
    +  real(kfp) :: delta(NN,1:3)
    +  real(kfp) :: dist2(NN)
       type(t_tree_node_interaction_data) :: node
       real*8 :: t1, t2
       integer :: i

    -  
    -  allocate(p%ex(NN), p%ey(NN), p%ez(NN), p%pot(NN), dist2(NN), delta(NN,1:3))
       dist2(:) = 1._kfp

       t1 = MPI_WTIME()
    @@ -104,6 +95,4 @@ program main
       t2 = MPI_WTIME()
       write(*,'("Total time: ", f12.8)') t2-t1

    -
    -  deallocate(p%ex, p%ey, p%ez, p%pot)
     end program

Again: no impact, no additional SIMDization inside the [compiler optimization listing](/dasmy/bgq_qpx/src/827f5128b0a547d71e3276afa9fa85bff089dcca/main.lst#cl-240).

The total runtime for this version is **2.40 seconds**.

### Using separate vectors instead of the structured datatype that contains them

Next assumption: Maybe, the fact that the vectors are folded into some structured datatype prevents autovectorization?
Result fields for `ex`, `ey`, `ez`, `pot` are handed to the kernel function explicitly instead.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/241a909fc1ee07ff96f208ec856ff08c06496c05#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -11,11 +11,11 @@ module bla
         real(kfp) :: eps = 0.1

           type t_particle_pack
    -         real*8 :: ex(NN)
    -         real*8 :: ey(NN)
    -         real*8 :: ez(NN)
    -         real*8 :: pot(NN)
    -         real*8 :: q(NN)
    +         real*8, allocatable :: ex(:)
    +         real*8, allocatable :: ey(:)
    +         real*8, allocatable :: ez(:)
    +         real*8, allocatable :: pot(:)
    +         real*8, allocatable :: q(:)
           end type t_particle_pack

           type t_tree_node_interaction_data
    @@ -35,12 +35,12 @@ module bla

       contains

    -    subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t, eps2)
    +    subroutine calc_force_coulomb_3D_direct(delta, dist2, ex, ey, ez, pot, t, eps2)
           implicit none
     !
           real(kfp), intent(in) :: delta(:,:)
           real(kfp), intent(in) :: dist2(:)
    -      type(t_particle_pack), intent(inout) :: particle_pack
    +      real(kfp), intent(inout) :: ex(:), ey(:), ez(:), pot(:)
           type(t_tree_node_interaction_data), intent(in) :: t
           real(kfp), intent(in) :: eps2
     !
    @@ -64,10 +64,10 @@ module bla
               rd = one/r
               rd3charge = t%charge*rd*rd*rd
     !
    -          particle_pack%pot(ip) = particle_pack%pot(ip) + mask(ip)*t%charge*rd
    -          particle_pack%ex(ip)  = particle_pack%ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    -          particle_pack%ey(ip)  = particle_pack%ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    -          particle_pack%ez(ip)  = particle_pack%ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
    +          pot(ip) = pot(ip) + mask(ip)*t%charge*rd
    +          ex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    +          ey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    +          ez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
           end do
         end subroutine calc_force_coulomb_3D_direct

    @@ -80,19 +80,23 @@ program main
       include 'mpif.h'

       type(t_particle_pack) :: p
    -  real(kfp) :: delta(NN,1:3)
    -  real(kfp) :: dist2(NN)
    +  real(kfp), allocatable :: delta(:,:)
    +  real(kfp), allocatable :: dist2(:)
       type(t_tree_node_interaction_data) :: node
       real*8 :: t1, t2
       integer :: i

    +  
    +  allocate(p%ex(NN), p%ey(NN), p%ez(NN), p%pot(NN), dist2(NN), delta(NN,1:3))
       dist2(:) = 1._kfp

       t1 = MPI_WTIME()
       do i=1,Niter
    -    call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
    +    call calc_force_coulomb_3D_direct(delta, dist2, p%ex, p%ey, p%ez, p%pot, node, eps)
       end do
       t2 = MPI_WTIME()
       write(*,'("Total time: ", f12.8)') t2-t1

    +
    +  deallocate(p%ex, p%ey, p%ez, p%pot)
     end program

This time a small runtime impact was observed.
This might be due to the much easier handling of the data inside the kernel.
However, no additional SIMDization but other reasons for that inside the [compiler optimization listing](/dasmy/bgq_qpx/src/241a909fc1ee07ff96f208ec856ff08c06496c05/main.lst#cl-869).

The total runtime for this version is **2.16 seconds**.


### Setting an explicit length for the supplied vectors

In some note (I think by Bob Walkup) on BG/P optimization I recognized that only explicit-shaped arrays were used in the examples.
Thus, switching from assumed shape (e.g. `dist2(:)` ) to these (`dist2(np)`) seemed to be worth a try.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/aa348fc372552972e60a64c162b937733e751cf1#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -35,22 +35,21 @@ module bla

       contains

    -    subroutine calc_force_coulomb_3D_direct(delta, dist2, ex, ey, ez, pot, t, eps2)
    +    subroutine calc_force_coulomb_3D_direct(np, delta, dist2, ex, ey, ez, pot, t, eps2)
           implicit none
     !
    -      real(kfp), intent(in) :: delta(:,:)
    -      real(kfp), intent(in) :: dist2(:)
    -      real(kfp), intent(inout) :: ex(:), ey(:), ez(:), pot(:)
    +      integer(kind_particle), intent(in) :: np
    +      real(kfp), intent(in) :: delta(np,3)
    +      real(kfp), intent(in) :: dist2(np)
    +      real(kfp), intent(inout) :: ex(np), ey(np), ez(np), pot(np)
           type(t_tree_node_interaction_data), intent(in) :: t
           real(kfp), intent(in) :: eps2
     !
           real(kfp) :: rd,r,rd3charge
    -      integer(kind_particle) :: ip, np
    +      integer(kind_particle) :: ip

           real(kfp) :: mask(size(dist2))
     !
    -      np = size(dist2, kind = kind_particle)
    -
           where (dist2 > 0.0_kfp)
             mask = 1._kfp
           elsewhere
    @@ -92,7 +91,7 @@ program main

       t1 = MPI_WTIME()
       do i=1,Niter
    -    call calc_force_coulomb_3D_direct(delta, dist2, p%ex, p%ey, p%ez, p%pot, node, eps)
    +    call calc_force_coulomb_3D_direct(size(dist2, kind = kind_particle), delta, dist2, p%ex, p%ey, p%ez, p%pot, node, eps)
       end do
       t2 = MPI_WTIME()
       write(*,'("Total time: ", f12.8)') t2-t1

Apparently, this did the trick (obvious, right?): The [compiler optimization
listing](/dasmy/bgq_qpx/src/aa348fc372552972e60a64c162b937733e751cf1/main.lst#cl-1300) now tells us about lots of successful SIMD
vectorization.
As a result, we are getting a factor of 2 in performance.
The reason for the speedup not being 4 may be that lots of "Vector versioning" was applied to some loops - whatever that is, we should get
rid of it.

The total runtime for this version is **1.37 seconds**.


### Replacing the `where`/`elsewhere` by a simple `where`

Assuming that the conditional jumps in the `where` statement for creating the mask could be a performance obstacle, I simplified it a bit.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/c2a6518509b639578f1c3f3e25c8bb64796a0efe#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -49,13 +49,12 @@ module bla
           integer(kind_particle) :: ip

           real(kfp) :: mask(size(dist2))
    +
    +      mask = 1._kfp
     !
    -      where (dist2 > 0.0_kfp)
    -        mask = 1._kfp
    -      elsewhere
    +      where (dist2 <= 0.0_kfp)
             mask = 0._kfp
           end where
    -
     !
           do ip = 1, np
     !

However, this did not really change anything as the compiler fused the implicit loop and the `where` statement again as can be seen in the [compiler optimization listing](/dasmy/bgq_qpx/src/c2a6518509b639578f1c3f3e25c8bb64796a0efe/main.lst#cl-1305).

The total runtime for this version is **1.31 seconds**.


### Temporary variables for the reduction

I actually do not remember why I tried this - it does not help at all :-)

The diff for this change is available [here](/dasmy/bgq_qpx/commits/ea85cb2d6f00f49b5592017a310fbc4e4d7bfe54#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -44,6 +44,7 @@ module bla
           real(kfp), intent(inout) :: ex(np), ey(np), ez(np), pot(np)
           type(t_tree_node_interaction_data), intent(in) :: t
           real(kfp), intent(in) :: eps2
    +      real(kfp) :: tex(np), tey(np), tez(np), tpot(np)
     !
           real(kfp) :: rd,r,rd3charge
           integer(kind_particle) :: ip
    @@ -62,11 +63,16 @@ module bla
               rd = one/r
               rd3charge = t%charge*rd*rd*rd
     !
    -          pot(ip) = pot(ip) + mask(ip)*t%charge*rd
    -          ex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    -          ey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    -          ez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
    +          tpot(ip) = pot(ip) + mask(ip)*t%charge*rd
    +          tex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    +          tey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    +          tez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
           end do
    +      
    +      pot = tpot
    +      ex  = tex
    +      ey  = tey
    +      ez  = tez
         end subroutine calc_force_coulomb_3D_direct

     end module bla

Furthermore, no obviously interesting things appeared in the [compiler optimization listing](/dasmy/bgq_qpx/src/ea85cb2d6f00f49b5592017a310fbc4e4d7bfe54/main.lst#cl-996).

The total runtime for this version is **8.17 seconds**.


### Revert temporary array commit

This commit just reverts the previous commit as it was not really helpful.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/45dbc542b604dc00e99690fe0c0c10c55ac8b492#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -44,35 +44,29 @@ module bla
           real(kfp), intent(inout) :: ex(np), ey(np), ez(np), pot(np)
           type(t_tree_node_interaction_data), intent(in) :: t
           real(kfp), intent(in) :: eps2
    -      real(kfp) :: tex(np), tey(np), tez(np), tpot(np)
     !
           real(kfp) :: rd,r,rd3charge
           integer(kind_particle) :: ip

    -      real(kfp) :: mask(size(dist2))
    +      real(kfp) :: mask(np)

           mask = 1._kfp
     !
           where (dist2 <= 0.0_kfp)
             mask = 0._kfp
           end where
    -!
    +
           do ip = 1, np
     !
               r = sqrt(dist2(ip) + eps2)
               rd = one/r
               rd3charge = t%charge*rd*rd*rd
     !
    -          tpot(ip) = pot(ip) + mask(ip)*t%charge*rd
    -          tex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    -          tey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    -          tez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
    +          pot(ip) = pot(ip) + mask(ip)*t%charge*rd
    +          ex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    +          ey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    +          ez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
           end do
    -      
    -      pot = tpot
    -      ex  = tex
    -      ey  = tey
    -      ez  = tez
         end subroutine calc_force_coulomb_3D_direct

     end module bla

As expected, the [compiler optimization listing](/dasmy/bgq_qpx/src/45dbc542b604dc00e99690fe0c0c10c55ac8b492/main.lst#cl-1304) is looking as before.

The total runtime for this version is **1.31 seconds**.


### Loop unrolling

Let's try to convince the compiler to actively perform loop unrolling.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/3ed86f7e2339e4dc1205eef7e2d515dc9d87fb60#chg-build.sh)

    :::diff
    --- a/build.sh
    +++ b/build.sh
    @@ -1,3 +1,3 @@
     #!/bin/bash

    -mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict main.f90
    +mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict -qunroll=yes main.f90

But the compiler does not care as can bee seen in the [compiler optimization listing](/dasmy/bgq_qpx/src/3ed86f7e2339e4dc1205eef7e2d515dc9d87fb60/main.lst#cl-1304).

The total runtime for this version is **1.30 seconds**.


### Adding alignment hints again.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/1479db9fb8bb4a18b84b7dbcc716a972ab3d274f#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -49,14 +49,22 @@ module bla
           integer(kind_particle) :: ip

           real(kfp) :: mask(np)
    -
    +!
           mask = 1._kfp
     !
           where (dist2 <= 0.0_kfp)
             mask = 0._kfp
           end where
    -
    -      do ip = 1, np
    +!
    +      call alignx(32, ex)
    +      call alignx(32, ey)
    +      call alignx(32, ez)
    +      call alignx(32, pot)
    +      call alignx(32, dist2)
    +      call alignx(32, delta)
    +!      
    +      !ibm* assert(nodeps,itercnt(NN))
    +      do ip=1,np
     !
               r = sqrt(dist2(ip) + eps2)
               rd = one/r

As before alignment hints did not make a considerable difference: [compiler optimization listing](/dasmy/bgq_qpx/src/1479db9fb8bb4a18b84b7dbcc716a972ab3d274f/main.lst#cl-1280).

The total runtime for this version is **1.32 seconds**.




****************************************




## Manual vectorization

Well - apparently automatic vectorization did not lead to the expected factor 4 in performance (the BG/Q chip has a quad floating point unit) or even close to that.
As visible in the compiler listings where autovectorization was successful, the loops are decorated with lots of [address computation, exception
handling for inappropriate vector lengths etc.](/dasmy/bgq_qpx/src/1479db9fb8bb4a18b84b7dbcc716a972ab3d274f/main.lst#cl-749)

Fortunately, the XLF-Compiler for BG/Q also supports vector intrinsics for manually performing vectorization of loops.
Details can be found in the [intrinsics section of the compiler reference](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/index.jsp?topic=%2Fcom.ibm.xlf141.bg.doc%2Flanguage_ref%2Fvmxintrinsics.html).

While these functions are first looking very cumbersome, applying them is actually not too difficult as will become clear with the following notes.


### Initial implementation of the QPX vectorized interaction

For clarity, we remove all alignment and loop control hints for now.

Essentially, the main loop over the individual interactions has to be replaced by a 4-step loop where every step computes 4 interactions.
It might seem obvious that the number of interactions to be computed has to be a multiple of 4 (we set `NN=512` instead of `500` here).

The (now 4-vector) temporary variables are instances of the XLF-specific datatype `VECTOR(REAL(8))`.
These can be initialized by a [`VEC_LDA`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_ld.html) instruction.
While using `VEC_LD` for now, in the final version of the code we should make sure to use `VEC_LDA` which generates a `SIGBUS` exception if data with inappropriate alignment is being
used.
In the documentation there are no hints about a potential performance penalty when using the safer `VEC_LDA` instead of `VEC_LD`.
Using a normal programming language like C, we as a programmer would have full control over alignment of our data.
In Fortran, alignment is neither guaranteed nor can it be controlled in any way.
Thus stick to the safe version or your code might compute garbage without any notification.
In addition, the exceptions generated by `VEC_LDA` can support debugging of loop indices etc.
Using [`VEC_STA`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_st.html) the 4-vectors can be stored back to memory.
The same comments also apply here.

For the instances of `VECTOR(REAL(8))`, regular algebra (`+`, `-`, `*`,`/`) is prohibited.
You have to use the compiler intrinsics instead and thus replace `*` by [`VEC_MUL`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_mul.html) etc.
Make sure that you first identify all opportunities of using [`VEC_MADD`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_madd.html) and
[`VEC_MSUB`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_msub.html) as these instructions can perform two floating point operations in a
single cycle.
For me, identifying them was easier when going through the original instructions from the very last towards the first term but this might strongly depend on your code.

As `VEC_LD` and `VEC_ST` are counting the start address offset in bytes.
Thus, for now it is more convenient to use `offset=0` here and directly put in the respective array slices.

A very nice QPX instruction for replacing our masking athletics is available with the combination of [`VEC_SEL`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_sel.html) and [`VEC_CMPGT`](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_cmpgt.html).
These allow for fully vectorized masking operations which apparently has not been possible with automatic vectorization.
(Attention: `VEC_SEL` is being used with wrong order of arguments here - will be corrected in the next commit.)

The diff for this change is available [here](/dasmy/bgq_qpx/commits/7e4de57ff64f6d2dd257e71aff71b734cbb5f43a#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -2,7 +2,7 @@
     module bla
       implicit none
       save
    -    integer, parameter :: NN = 500
    +    integer, parameter :: NN = 512
         integer, parameter :: Niter = 100000
     !
         integer, parameter :: kfp = 8 ! numeric precision (kind value)
    @@ -45,36 +45,31 @@ module bla
           type(t_tree_node_interaction_data), intent(in) :: t
           real(kfp), intent(in) :: eps2
     !
    -      real(kfp) :: rd,r,rd3charge
    +      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vzero, vdist2, vone, vtcharge, veps2
           integer(kind_particle) :: ip
    -      
    -      real(kfp) :: mask(np)
    -!
    -      mask = 1._kfp
    -!
    -      where (dist2 <= 0.0_kfp)
    -        mask = 0._kfp
    -      end where
     !
    -      call alignx(32, ex)
    -      call alignx(32, ey)
    -      call alignx(32, ez)
    -      call alignx(32, pot)
    -      call alignx(32, dist2)
    -      call alignx(32, delta)
    +      vzero    = VEC_SPLATS(0._kfp)
    +      vone     = VEC_SPLATS(one)
    +      vtcharge = VEC_SPLATS(t%charge)
    +      veps2    = VEC_SPLATS(eps2)
     !      
    -      !ibm* assert(nodeps,itercnt(NN))
    -      do ip=1,np
    +      !ibm* assert(nodeps)
    +      do ip=0,np-1,4
    +          vdist2 = VEC_LD(ip, dist2)
     !
    -          r = sqrt(dist2(ip) + eps2)
    -          rd = one/r
    -          rd3charge = t%charge*rd*rd*rd
    +          r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
    +          rd = VEC_SEL(VEC_SWDIV(vone, r), vzero, VEC_CMPGT(vdist2, vzero))
     !
    -          pot(ip) = pot(ip) + mask(ip)*t%charge*rd
    -          ex(ip)  = ex(ip)  + mask(ip)*rd3charge*delta(ip,1)
    -          ey(ip)  = ey(ip)  + mask(ip)*rd3charge*delta(ip,2)
    -          ez(ip)  = ez(ip)  + mask(ip)*rd3charge*delta(ip,3)
    +          rdtcharge  = VEC_MUL(vtcharge,rd)
    +          rd3tcharge = VEC_MUL(rdtcharge, VEC_MUL(rd,rd))
    +! 
    +          call VEC_ST(VEC_ADD(rdtcharge, VEC_LD(ip, pot)), ip, pot)
    +
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,1)), VEC_LD(ip, ex)), ip, ex)
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,2)), VEC_LD(ip, ey)), ip, ey)
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,3)), VEC_LD(ip, ez)), ip, ez)
           end do
    +      
         end subroutine calc_force_coulomb_3D_direct

     end module bla


The [compiler optimization listing](/dasmy/bgq_qpx/src/7e4de57ff64f6d2dd257e71aff71b734cbb5f43a/main.lst#cl-666) shows that our code completely survived almost without any changes.
In comparison to previous version, there will be no surprise about it being considerably faster already due to being significantly shorter.

The total runtime for this version is **0.60 seconds**.


### Going back to the packed datatype (struct of arrays) again

As we are not restricted by the peculiarities for automatic vectorization any more, the first logical step is to head back towards the original datatypes and perform some code cleanup.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/9d8902d4f61f9c548c9c90808d4a0396a5376f3b#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -7,8 +7,9 @@ module bla
     !
         integer, parameter :: kfp = 8 ! numeric precision (kind value)
         integer, public, parameter :: kind_particle     = kind(1_8) ! ATTENTION, see above
    -    real(kfp), parameter :: one = 1._kfp
         real(kfp) :: eps = 0.1
    +    
    +    VECTOR(REAL(kfp)) :: vzero, vone, veps2

           type t_particle_pack
              real*8, allocatable :: ex(:)
    @@ -35,39 +36,35 @@ module bla

       contains

    -    subroutine calc_force_coulomb_3D_direct(np, delta, dist2, ex, ey, ez, pot, t, eps2)
    +    subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t)
           implicit none
     !
    -      integer(kind_particle), intent(in) :: np
    -      real(kfp), intent(in) :: delta(np,3)
    -      real(kfp), intent(in) :: dist2(np)
    -      real(kfp), intent(inout) :: ex(np), ey(np), ez(np), pot(np)
    +      real(kfp), intent(in) :: delta(:,:)
    +      real(kfp), intent(in) :: dist2(:)
    +      type(t_particle_pack), intent(inout) :: particle_pack
           type(t_tree_node_interaction_data), intent(in) :: t
    -      real(kfp), intent(in) :: eps2
     !
    -      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vzero, vdist2, vone, vtcharge, veps2
    -      integer(kind_particle) :: ip
    +      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vdist2, vtcharge
    +      integer(kind_particle) :: ip, np
     !
    -      vzero    = VEC_SPLATS(0._kfp)
    -      vone     = VEC_SPLATS(one)
           vtcharge = VEC_SPLATS(t%charge)
    -      veps2    = VEC_SPLATS(eps2)
     !      
           !ibm* assert(nodeps)
    -      do ip=0,np-1,4
    +      do ip=0,size(dist2, kind = kind_particle)-1,4
               vdist2 = VEC_LD(ip, dist2)
     !
    +          ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
               r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
    -          rd = VEC_SEL(VEC_SWDIV(vone, r), vzero, VEC_CMPGT(vdist2, vzero))
    +          rd = VEC_SEL(vzero, VEC_SWDIV(vone, r), VEC_CMPGT(vdist2, vzero))
     !
               rdtcharge  = VEC_MUL(vtcharge,rd)
               rd3tcharge = VEC_MUL(rdtcharge, VEC_MUL(rd,rd))
     ! 
    -          call VEC_ST(VEC_ADD(rdtcharge, VEC_LD(ip, pot)), ip, pot)
    +          call VEC_ST(VEC_ADD(rdtcharge, VEC_LD(ip, particle_pack%pot)), ip, particle_pack%pot)

    -          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,1)), VEC_LD(ip, ex)), ip, ex)
    -          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,2)), VEC_LD(ip, ey)), ip, ey)
    -          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,3)), VEC_LD(ip, ez)), ip, ez)
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,1)), VEC_LD(ip, particle_pack%ex)), ip, particle_pack%ex)
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,2)), VEC_LD(ip, particle_pack%ey)), ip, particle_pack%ey)
    +          call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,3)), VEC_LD(ip, particle_pack%ez)), ip, particle_pack%ez)
           end do

         end subroutine calc_force_coulomb_3D_direct
    @@ -92,8 +89,13 @@ program main
       dist2(:) = 1._kfp

       t1 = MPI_WTIME()
    +
    +  vzero    = VEC_SPLATS(0._kfp)
    +  vone     = VEC_SPLATS(1._kfp)
    +  veps2    = VEC_SPLATS(eps)
    +
       do i=1,Niter
    -    call calc_force_coulomb_3D_direct(size(dist2, kind = kind_particle), delta, dist2, p%ex, p%ey, p%ez, p%pot, node, eps)
    +    call calc_force_coulomb_3D_direct(delta, dist2, p, node)
       end do
       t2 = MPI_WTIME()
       write(*,'("Total time: ", f12.8)') t2-t1


The [compiler optimization listing](/dasmy/bgq_qpx/src/9d8902d4f61f9c548c9c90808d4a0396a5376f3b/main.lst#cl-666) is looking very similar to before but still contains a lot of address
computation that we will try to get rid of later.

The total runtime for this version is **0.61 seconds**.



### `VEC_SWSQRT_NOCHK` vs `VEC_SWSQRT`

The [vectorized square root](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_swsqrt.html) is available in two versions with different ranges of arguments allowed.
While in our example we can essentially guarantee to stick to the tighter range, I was interested in the performance impact of either choice.
This is - besides some minor reorganization - the primary change in this commit. 

The diff for this change is available [here](/dasmy/bgq_qpx/commits/98c4965dd048e949b819d7f1428ee969ebba373b#chg-main.f90)

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -7,9 +7,8 @@ module bla
     !
         integer, parameter :: kfp = 8 ! numeric precision (kind value)
         integer, public, parameter :: kind_particle     = kind(1_8) ! ATTENTION, see above
    +    real(kfp), parameter :: one = 1._kfp
         real(kfp) :: eps = 0.1
    -    
    -    VECTOR(REAL(kfp)) :: vzero, vone, veps2

           type t_particle_pack
              real*8, allocatable :: ex(:)
    @@ -36,32 +35,35 @@ module bla

       contains

    -    subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t)
    +    subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t, eps2)
           implicit none
     !
           real(kfp), intent(in) :: delta(:,:)
           real(kfp), intent(in) :: dist2(:)
           type(t_particle_pack), intent(inout) :: particle_pack
           type(t_tree_node_interaction_data), intent(in) :: t
    +      real(kfp), intent(in) :: eps2
     !
    -      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vdist2, vtcharge
    +      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vzero, vdist2, vone, vtcharge, veps2
           integer(kind_particle) :: ip, np
     !
    +      vzero    = VEC_SPLATS(0._kfp)
    +      vone     = VEC_SPLATS(one)
           vtcharge = VEC_SPLATS(t%charge)
    +      veps2    = VEC_SPLATS(eps2)
     !      
           !ibm* assert(nodeps)
           do ip=0,size(dist2, kind = kind_particle)-1,4
               vdist2 = VEC_LD(ip, dist2)
     !
               ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
    -          r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
    +          r  = VEC_SWSQRT( VEC_ADD(vdist2, veps2) )
               rd = VEC_SEL(vzero, VEC_SWDIV(vone, r), VEC_CMPGT(vdist2, vzero))
     !
               rdtcharge  = VEC_MUL(vtcharge,rd)
    -          rd3tcharge = VEC_MUL(rdtcharge, VEC_MUL(rd,rd))
    -! 
               call VEC_ST(VEC_ADD(rdtcharge, VEC_LD(ip, particle_pack%pot)), ip, particle_pack%pot)
    -
    +!
    +          rd3tcharge = VEC_MUL(rdtcharge, VEC_MUL(rd,rd))
               call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,1)), VEC_LD(ip, particle_pack%ex)), ip, particle_pack%ex)
               call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,2)), VEC_LD(ip, particle_pack%ey)), ip, particle_pack%ey)
               call VEC_ST(VEC_MADD(rd3tcharge, VEC_LD(0, delta(ip+1,3)), VEC_LD(ip, particle_pack%ez)), ip, particle_pack%ez)
    @@ -89,13 +91,8 @@ program main
       dist2(:) = 1._kfp

       t1 = MPI_WTIME()
    -
    -  vzero    = VEC_SPLATS(0._kfp)
    -  vone     = VEC_SPLATS(1._kfp)
    -  veps2    = VEC_SPLATS(eps)
    -
       do i=1,Niter
    -    call calc_force_coulomb_3D_direct(delta, dist2, p, node)
    +    call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
       end do
       t2 = MPI_WTIME()
       write(*,'("Total time: ", f12.8)') t2-t1

No surprises in the [compiler optimization listing](/dasmy/bgq_qpx/src/98c4965dd048e949b819d7f1428ee969ebba373b/main.lst#cl-666).

The total runtime for this version is ten percent longer: **0.66 seconds**.



### Impact of `qnostrict`

As in a previous commit adding `qnostrict` made a big difference in runtime, we are removing it here to see if this is still the case.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/5d8f4765670dfb5673a3d93fd344649d5c235640#chg-build.sh)

    :::diff
    --- a/build.sh
    +++ b/build.sh
    @@ -1,3 +1,3 @@
     #!/bin/bash

    -mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict -qunroll=yes main.f90
    +mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qunroll=yes main.f90


    --- a/main.f90
    +++ b/main.f90
    @@ -57,7 +57,7 @@ module bla
               vdist2 = VEC_LD(ip, dist2)
     !
               ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
    -          r  = VEC_SWSQRT( VEC_ADD(vdist2, veps2) )
    +          r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
               rd = VEC_SEL(vzero, VEC_SWDIV(vone, r), VEC_CMPGT(vdist2, vzero))
     !
               rdtcharge  = VEC_MUL(vtcharge,rd)

While the [compiler optimization listing](/dasmy/bgq_qpx/src/5d8f4765670dfb5673a3d93fd344649d5c235640/main.lst#cl-671) did not change much, the overall program has become considerably
slower.

The total runtime for this version is **2.64 seconds**.




### `VEC_SWDIV_NOCHK` vs `VEC_SWDIV`

Very similar to the square root, the [vectorized division](http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vec_swdiv.html) is also available in two different versions with and without input range checking.
Again, using the version without input range checking makes a considerable difference in runtime (but we as the user have to assure a valid parameter range which we simply assume
due to our physical problem).

The diff for this change is available [here](/dasmy/bgq_qpx/commits/bb397ca7e3dd59d13a4818c141d29067c8282c15#chg-build.sh)

    :::diff
    --- a/build.sh
    +++ b/build.sh
    @@ -1,3 +1,3 @@
     #!/bin/bash

    -mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qunroll=yes main.f90
    +mpixlf90_r -O5 -qnounwind -qhot -qipa=level=1 -qlibmpi -qinline -qinfo=all -qstrict -qreport -qthreaded -qsmp=noauto -qnostrict -qunroll=yes main.f90


    --- a/main.f90
    +++ b/main.f90
    @@ -58,7 +58,7 @@ module bla
     !
               ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
               r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
    -          rd = VEC_SEL(vzero, VEC_SWDIV(vone, r), VEC_CMPGT(vdist2, vzero))
    +          rd = VEC_SEL(vzero, VEC_SWDIV_NOCHK(vone, r), VEC_CMPGT(vdist2, vzero))
     !
               rdtcharge  = VEC_MUL(vtcharge,rd)
               call VEC_ST(VEC_ADD(rdtcharge, VEC_LD(ip, particle_pack%pot)), ip, particle_pack%pot)


Again: no changes to the [compiler optimization listing](/dasmy/bgq_qpx/src/bb397ca7e3dd59d13a4818c141d29067c8282c15/main.lst#cl-666).

The total runtime for this version is **0.48 seconds**.




****************************************




## Full multipole interaction and verification of results

Now, that we assume to have a very fast version of our interaction kernel, it might be important to know if it is still correct.
Therefore, a full program is being developed that includes the original as well as the manually vectorized version, measures their runtime and compares the results.

### Full test application

As the direct particle-particle interaction was not the only relevant interaction routine but only a smaller one, I also added the particle-multipole interaction here and prepared a
QPX-vectorized version of that.

The diff for this change is available [here](/dasmy/bgq_qpx/commits/51b8a7f06bbbe68f0d676468486c7a892d774994#chg-main.f90).
To improve readability, some preprocessor macros were introduced, e.g. `VEC_MUL3()` for pointwise multiplying 3 vectors instead of only 2 etc.

The new full test case now reads

    :::fortran
    module bla
      implicit none
      save
        integer, parameter :: NN = 512
        integer, parameter :: Niter = 100000
    !
        integer, parameter :: kfp = 8 ! numeric precision (kind value)
        integer, public, parameter :: kind_particle     = kind(1_8) ! ATTENTION, see above
        real(kfp), parameter :: zero    =  0._kfp
        real(kfp), parameter :: three   =  3._kfp
        real(kfp), parameter :: one     =  1._kfp
        real(kfp), parameter :: five    =  5._kfp
        real(kfp), parameter :: half    =  0.5_kfp
        real(kfp) :: eps = 0.1

          type t_particle_pack
             real*8, allocatable :: ex(:)
             real*8, allocatable :: ey(:)
             real*8, allocatable :: ez(:)
             real*8, allocatable :: pot(:)
          end type t_particle_pack

          type t_tree_node_interaction_data
            real*8 :: charge ! net charge sum
            real*8 :: dip(3) ! dipole moment
            real*8 :: quad(3) ! diagonal quadrupole moments
            real*8 :: xyquad ! other quadrupole moments
            real*8 :: yzquad
            real*8 :: zxquad
          end type t_tree_node_interaction_data


        public calc_force_coulomb_3D_direct
        public calc_force_coulomb_3D_direct_reference
        public clear_results
        public rel_err_norm

      contains

        ! a * b * c
        #define VEC_MUL3(a,b,c) VEC_MUL(a, VEC_MUL(b,c))
        ! a * b + c * d
        #define VEC_aTbMcTd(a,b,c,d) VEC_MSUB(a,b,VEC_MUL(c,d))
        ! a * b + c * d + e * f
        #define VEC_aTbPcTdPeTf(a,b,c,d,e,f) VEC_MADD(a,b,VEC_MADD(c,d,VEC_MUL(e,f)))
        ! a * b * c -d
        #define VEC_MUL3SUB(a,b,c,d) VEC_MSUB(a, VEC_MUL(b,c), d)
        ! performs variable(index) += summand
        #define VEC_ACCUMULATE(index, variable, summand) VEC_STA(VEC_ADD(VEC_LDA(index,variable),summand),index,variable)
        ! performs variable(index) += a * bsummand
        #define VEC_MACCUMULATE(index, variable, a, b) VEC_STA(VEC_MADD(a,b,VEC_LDA(index,variable)),index,variable)


        subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t, eps2)
          implicit none

          real(kfp), intent(in) :: delta(:,:)
          real(kfp), intent(in) :: dist2(:)
          type(t_particle_pack), intent(inout) :: particle_pack
          type(t_tree_node_interaction_data), intent(in) :: t
          real(kfp), intent(in) :: eps2

          VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vzero, vdist2, vone, vtcharge, veps2
          integer(kind_particle) :: ip, np

          vzero    = VEC_SPLATS(zero)
          vone     = VEC_SPLATS(one)
          vtcharge = VEC_SPLATS(t%charge)
          veps2    = VEC_SPLATS(eps2)

          np = 8*size(dist2, kind = kind_particle)

          !ibm* assert(nodeps)
          do ip=0,np-8,32
              vdist2 = VEC_LDA(ip, dist2)

              ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
              r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
              rd = VEC_SEL(vzero, VEC_SWDIV_NOCHK(vone, r), VEC_CMPGT(vdist2, vzero))

              rdtcharge  = VEC_MUL(vtcharge, rd)
              call VEC_ACCUMULATE(ip, particle_pack%pot, rdtcharge)
              rd3tcharge = VEC_MUL3(rdtcharge,rd,rd)
              call VEC_MACCUMULATE(ip, particle_pack%ex, rd3tcharge, VEC_LDA(      ip, delta))
              call VEC_MACCUMULATE(ip, particle_pack%ey, rd3tcharge, VEC_LDA(   np+ip, delta))
              call VEC_MACCUMULATE(ip, particle_pack%ez, rd3tcharge, VEC_LDA(np+np+ip, delta))
          end do

        end subroutine calc_force_coulomb_3D_direct

        subroutine calc_force_coulomb_3D_direct_reference(delta, dist2, particle_pack, t, eps2)
          implicit none

          real(kfp), intent(in) :: delta(:,:)
          real(kfp), intent(in) :: dist2(:)
          type(t_particle_pack), intent(inout) :: particle_pack
          type(t_tree_node_interaction_data), intent(in) :: t
          real(kfp), intent(in) :: eps2

          real(kfp) :: rd,r,rd3charge
          integer(kind_particle) :: ip, np

          np = size(dist2, kind = kind_particle)

          do ip = 1, np

            if (dist2(ip) > zero) then
              r = sqrt(dist2(ip) + eps2)
              rd = one/r
              rd3charge = t%charge*rd*rd*rd

              particle_pack%pot(ip) = particle_pack%pot(ip) + t%charge*rd
              particle_pack%ex(ip) = particle_pack%ex(ip) + rd3charge*delta(ip,1)
              particle_pack%ey(ip) = particle_pack%ey(ip) + rd3charge*delta(ip,2)
              particle_pack%ez(ip) = particle_pack%ez(ip) + rd3charge*delta(ip,3)
            end if
          end do
        end subroutine calc_force_coulomb_3D_direct_reference

        subroutine calc_force_coulomb_3D(delta, dist2, particle_pack, t, eps2)
          implicit none

          real(kfp), intent(in) :: delta(:,:)
          real(kfp), intent(in) :: dist2(:)
          type(t_particle_pack), intent(inout) :: particle_pack
          type(t_tree_node_interaction_data), intent(in) :: t
          real(kfp), intent(in) :: eps2

          vector(real(kfp)) :: rd,dx,dy,dz,dx2,dy2,dz2,rd2,rd3,rd5,rd7,fd1,fd2,fd3,fd4,fd5,fd6
          vector(real(kfp)) :: veps2, vone, vthree, vfive, vhalf, vdist2, vtcharge, vtdip(3), vtquad(3), vtxyquad, vtzxquad, vtyzquad, &
                               vfive_rd7, dx_rd5, dy_rd5, dz_rd5, vtcharge_rd3, vfive_rd7_dx, vfive_rd7_dy, vfive_rd7_dz,vfive_rd7_dxdydz
          integer(kind_particle) :: ip, np

          veps2    = VEC_SPLATS(eps2)
          vone     = VEC_SPLATS(one)
          vthree   = VEC_SPLATS(three)
          vfive    = VEC_SPLATS(five)
          vhalf    = VEC_SPLATS(half)

          vtcharge = VEC_SPLATS(t%charge)
          do ip=1,3
            vtdip(ip)  = VEC_SPLATS(t%dip(ip))
            vtquad(ip) = VEC_SPLATS(t%quad(ip))
          end do
          vtxyquad = VEC_SPLATS(t%xyquad)
          vtyzquad = VEC_SPLATS(t%yzquad)
          vtzxquad = VEC_SPLATS(t%zxquad)

          np = 8*size(dist2, kind = kind_particle)

          !ibm* assert(nodeps)
          do ip=0,np-8,32
            ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
            vdist2 = VEC_LDA(ip, dist2)

            rd  = VEC_SWDIV_NOCHK(vone, VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) ))
            rd2 = VEC_MUL(rd,  rd )
            rd3 = VEC_MUL(rd,  rd2)
            rd5 = VEC_MUL(rd3, rd2)
            rd7 = VEC_MUL(rd5, rd2)

            dx = VEC_LDA(      ip, delta)
            dy = VEC_LDA(   np+ip, delta)
            dz = VEC_LDA(np+np+ip, delta)
            dx2 = VEC_MUL(dx, dx)
            dy2 = VEC_MUL(dy, dy)
            dz2 = VEC_MUL(dz, dz)

            fd1 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dx2), rd5), rd3)
            fd2 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dy2), rd5), rd3)
            fd3 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dz2), rd5), rd3)
            fd4 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dx), dy), rd5)
            fd5 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dy), dz), rd5)
            fd6 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dx), dz), rd5)

            vfive_rd7    = VEC_MUL(vfive, rd7)
            vfive_rd7_dx = VEC_MUL(vfive_rd7, dx)
            vfive_rd7_dy = VEC_MUL(vfive_rd7, dy)
            vfive_rd7_dz = VEC_MUL(vfive_rd7, dz)
            vfive_rd7_dxdydz = VEC_MUL3(vfive_rd7_dx, dy, dz)
            dx_rd5       = VEC_MUL(dx, rd5)
            dy_rd5       = VEC_MUL(dy, rd5)
            dz_rd5       = VEC_MUL(dz, rd5)
            vtcharge_rd3 = VEC_MUL(vtcharge, rd3)

            call VEC_ACCUMULATE(ip, particle_pack%pot,
                    VEC_MADD(vtcharge,
                             rd,
                             VEC_MADD(rd3,
                                      VEC_aTbPcTdPeTf(dx,vtdip(1),dy,vtdip(2),dz,vtdip(3)),
                                      VEC_MADD(vhalf,
                                               VEC_aTbPcTdPeTf(fd1,vtquad(1),fd2,vtquad(2),fd3,vtquad(3)),
                                               VEC_aTbPcTdPeTf(fd4,vtxyquad,fd5,vtyzquad,fd6,vtzxquad)
                                               )
                                      )
                             )
                            )

            call VEC_ACCUMULATE(ip,particle_pack%ex,
                      VEC_MADD( vthree,
                                VEC_MADD( vhalf,
                                          VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dx,dx2,dx_rd5,vthree),vtquad(1),
                                                          VEC_MSUB(   vfive_rd7_dx,dy2,dx_rd5)       ,vtquad(2),
                                                          VEC_MSUB(   vfive_rd7_dx,dz2,dx_rd5)       ,vtquad(3)
                                                          ),
                                          VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dy,dx2,dy_rd5), vtxyquad,
                                                          VEC_MSUB(vfive_rd7_dz,dx2,dz_rd5), vtzxquad,
                                                          vfive_rd7_dxdydz,                  vtyzquad
                                                          ) 
                                          ),
                                VEC_MADD( vtcharge_rd3,
                                          dx,
                                          VEC_aTbPcTdPeTf(fd1,vtdip(1),
                                                          fd4,vtdip(2),
                                                          fd6,vtdip(3))
                                         )
                               )
                              )

            call VEC_ACCUMULATE(ip,particle_pack%ey,
                      VEC_MADD( vthree,
                                VEC_MADD( vhalf,
                                          VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dy,dy2,dy_rd5,vthree),vtquad(2),
                                                          VEC_MSUB(   vfive_rd7_dy,dx2,dy_rd5)       ,vtquad(1),
                                                          VEC_MSUB(   vfive_rd7_dy,dz2,dy_rd5)       ,vtquad(3)
                                                          ),
                                          VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dx,dy2,dx_rd5 ),vtxyquad, 
                                                          VEC_MSUB(vfive_rd7_dz,dy2,dz_rd5 ),vtyzquad,
                                                          vfive_rd7_dxdydz,                  vtzxquad
                                                          )
                                          ),
                                VEC_MADD(vtcharge_rd3,
                                         dy,
                                         VEC_aTbPcTdPeTf(fd2,vtdip(2),
                                                         fd4,vtdip(1),
                                                         fd5,vtdip(3))
                                         )
                              )
                             )

            call VEC_ACCUMULATE(ip,particle_pack%ez,
                      VEC_MADD( vthree,
                                VEC_MADD( vhalf,
                                          VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dz,dz2,dz_rd5,vthree),vtquad(3),
                                                          VEC_MSUB(   vfive_rd7_dz,dy2,dz_rd5)       ,vtquad(2),
                                                          VEC_MSUB(   vfive_rd7_dz,dx2,dz_rd5)       ,vtquad(1)
                                                          ),
                                          VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dx,dz2,dx_rd5 ),vtzxquad, 
                                                          VEC_MSUB(vfive_rd7_dy,dz2,dy_rd5 ),vtyzquad,
                                                          vfive_rd7_dxdydz,                  vtxyquad
                                                          )
                                          ),
                                VEC_MADD(vtcharge_rd3,
                                         dz,
                                         VEC_aTbPcTdPeTf(fd3,vtdip(3),
                                                         fd5,vtdip(2),
                                                         fd6,vtdip(1))
                                         )
                              )
                             )
          end do
        end subroutine calc_force_coulomb_3D

        subroutine calc_force_coulomb_3D_reference(delta, dist2, particle_pack, t, eps2)
          implicit none

          real(kfp), intent(in) :: delta(:,:)
          real(kfp), intent(in) :: dist2(:)
          type(t_particle_pack), intent(inout) :: particle_pack
          type(t_tree_node_interaction_data), intent(in) :: t
          real(kfp), intent(in) :: eps2

          real(kfp) :: rd,dx,dy,dz,r,dx2,dy2,dz2,dx3,dy3,dz3,rd2,rd3,rd5,rd7,fd1,fd2,fd3,fd4,fd5,fd6
          integer(kind_particle) :: ip, np

          np = size(dist2, kind = kind_particle)

          do ip = 1, np
            dx = delta(ip, 1)
            dy = delta(ip, 2)
            dz = delta(ip, 3)

            r  = sqrt(dist2(ip) + eps2)
            rd = one/r
            rd2 = rd *rd
            rd3 = rd *rd2
            rd5 = rd3*rd2
            rd7 = rd5*rd2

            dx2 = dx*dx
            dy2 = dy*dy
            dz2 = dz*dz
            dx3 = dx*dx2
            dy3 = dy*dy2
            dz3 = dz*dz2

            fd1 = three*dx2*rd5 - rd3
            fd2 = three*dy2*rd5 - rd3
            fd3 = three*dz2*rd5 - rd3
            fd4 = three*dx*dy*rd5
            fd5 = three*dy*dz*rd5
            fd6 = three*dx*dz*rd5

            particle_pack%pot(ip) = particle_pack%pot(ip) &
                  + t%charge*rd                                           &  !  monopole term
                  + (dx*t%dip(1) + dy*t%dip(2) + dz*t%dip(3))*rd3         &  !  dipole
                  + half*(fd1*t%quad(1) + fd2*t%quad(2) + fd3*t%quad(3))  &  !  quadrupole
                  +       fd4*t%xyquad  + fd5*t%yzquad  + fd6*t%zxquad

            particle_pack%ex(ip) = particle_pack%ex(ip) &
                      + t%charge*dx*rd3                                  &  ! monopole term
                      + fd1*t%dip(1) + fd4*t%dip(2) + fd6*t%dip(3)       &  ! dipole term
                      + three * (                                        &  ! quadrupole term
                        half * (                                         &
                            ( five*dx3   *rd7 - three*dx*rd5 )*t%quad(1) &
                          + ( five*dx*dy2*rd7 -       dx*rd5 )*t%quad(2) &
                          + ( five*dx*dz2*rd7 -       dx*rd5 )*t%quad(3) &
                        )                                                &
                        + ( five*dy*dx2  *rd7 - dy*rd5 )*t%xyquad        &
                        + ( five*dz*dx2  *rd7 - dz*rd5 )*t%zxquad        &
                        + ( five*dx*dy*dz*rd7          )*t%yzquad        &
                        )

            particle_pack%ey(ip) = particle_pack%ey(ip) &
                      + t%charge*dy*rd3                                  &
                      + fd2*t%dip(2) + fd4*t%dip(1) + fd5*t%dip(3)       &
                      + three * (                                        &
                        half * (                                         &
                            ( five*dy3*rd7    - three*dy*rd5 )*t%quad(2) &
                          + ( five*dy*dx2*rd7 -       dy*rd5 )*t%quad(1) &
                          + ( five*dy*dz2*rd7 -       dy*rd5 )*t%quad(3) &
                        )                                                &
                        + ( five*dx*dy2  *rd7 - dx*rd5 )*t%xyquad        &
                        + ( five*dz*dy2  *rd7 - dz*rd5 )*t%yzquad        &
                        + ( five*dx*dy*dz*rd7          )*t%zxquad        &
                        )

            particle_pack%ez(ip) = particle_pack%ez(ip) &
                      + t%charge*dz*rd3                                  &
                      + fd3*t%dip(3) + fd5*t%dip(2) + fd6*t%dip(1)       &
                      + three * (                                        &
                        half * (                                         &
                          + ( five*dz3   *rd7 - three*dz*rd5 )*t%quad(3) &
                          + ( five*dz*dy2*rd7 -       dz*rd5 )*t%quad(2) &
                          + ( five*dz*dx2*rd7 -       dz*rd5 )*t%quad(1) &
                        )                                                &
                        + ( five*dx*dz2  *rd7 - dx*rd5 )*t%zxquad        &
                        + ( five*dy*dz2  *rd7 - dy*rd5 )*t%yzquad        &
                        + ( five*dx*dy*dz*rd7          )*t%xyquad        &
                        )
          end do
        end subroutine calc_force_coulomb_3D_reference

        subroutine clear_results(p)
          implicit none
          type(t_particle_pack), intent(inout) :: p

          p%ex  = zero
          p%ey  = zero
          p%ez  = zero
          p%pot = zero

        end subroutine

        real(kfp) function rel_err_norm(f1, f2)
          implicit none
          real(kfp), intent(in) :: f1(:), f2(:)
          rel_err_norm = maxval(abs((f1-f2)/f2))
        end function

    end module bla


    program main
      use bla
      implicit none
      include 'mpif.h'

      type(t_particle_pack) :: p, p_ref
      real(kfp), allocatable :: delta(:,:)
      real(kfp), allocatable :: dist2(:)
      type(t_tree_node_interaction_data) :: node
      real*8 :: t1, t2
      integer :: i


      allocate(p%ex(NN), p%ey(NN), p%ez(NN), p%pot(NN))
      allocate(p_ref%ex(NN), p_ref%ey(NN), p_ref%ez(NN), p_ref%pot(NN))
      allocate(dist2(NN), delta(NN,1:3))

      call random_number(dist2)
      call random_number(delta)
      call random_number(node%charge)
      call random_number(node%dip)
      call random_number(node%quad)
      call random_number(node%xyquad)
      call random_number(node%yzquad)
      call random_number(node%zxquad)

      write(*,*) '================= testing direct interaction ==============='
      call clear_results(p)
      call clear_results(p_ref)
      t1 = MPI_WTIME()
      do i=1,Niter
        call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
      end do
      t2 = MPI_WTIME()
      write(*,'("Total time:     ", f12.8)') t2-t1

      t1 = MPI_WTIME()
      do i=1,Niter
        call calc_force_coulomb_3D_direct_reference(delta, dist2, p_ref, node, eps)
      end do
      t2 = MPI_WTIME()
      write(*,'("Reference time: ", f12.8)') t2-t1

      write(*,*) 'rel error ex :', rel_err_norm(p%ex, p_ref%ex)
      write(*,*) 'rel error ey :', rel_err_norm(p%ey, p_ref%ey)
      write(*,*) 'rel error ez :', rel_err_norm(p%ez, p_ref%ez)
      write(*,*) 'rel error pot:', rel_err_norm(p%pot, p_ref%pot)

      write(*,*) '================= testing multipole interaction ==============='
      call clear_results(p)
      call clear_results(p_ref)
      t1 = MPI_WTIME()
      do i=1,Niter
        call calc_force_coulomb_3D(delta, dist2, p, node, eps)
      end do
      t2 = MPI_WTIME()
      write(*,'("Total time:     ", f12.8)') t2-t1

      t1 = MPI_WTIME()
      do i=1,Niter
        call calc_force_coulomb_3D_reference(delta, dist2, p_ref, node, eps)
      end do
      t2 = MPI_WTIME()
      write(*,'("Reference time: ", f12.8)') t2-t1

      write(*,*) 'rel error ex :', rel_err_norm(p%ex, p_ref%ex)
      write(*,*) 'rel error ey :', rel_err_norm(p%ey, p_ref%ey)
      write(*,*) 'rel error ez :', rel_err_norm(p%ez, p_ref%ez)
      write(*,*) 'rel error pot:', rel_err_norm(p%pot, p_ref%pot)


      deallocate(p%ex, p%ey, p%ez, p%pot)
      deallocate(p_ref%ex, p_ref%ey, p_ref%ez, p_ref%pot)
      deallocate(dist2, delta)

    end program

The total runtime and error data for this version are:


================= | testing direct interaction | ===============
----------------- | -------------------------- | ---------------
Total time:       | 0.54533962                 |
Reference time:   | 2.52298499                 |
rel error ex :    | 0.132315789442848879E-14   |
rel error ey :    | 0.128566288289644258E-14   |
rel error ez :    | 0.130324346644842082E-14   |
rel error pot:    | 0.358583900288934791E-15   |

================= | testing multipole interaction | ===============
----------------- | ----------------------------- | ---------------
Total time:       | 3.28456848                    |
Reference time:   | 9.10577225                    |
rel error ex :    | 0.322933960639945863E-08      |
rel error ey :    | 0.210398211545006775E-08      |
rel error ez :    | 0.203867420396472677E-08      |
rel error pot:    | 0.248193711299472289E-09      |

Well, this is promising. Now we have to put everything into the actual application...

### Measuring the error of a single iteration

The above program measures the accumulated error of `Niter` repetitions of the interaction kernels. The error for a single iteration is shown below.

================= | testing direct interaction | ===============
----------------- | -------------------------- | ---------------
Total time:       | 0.62131173                 |
Reference time:   | 2.51100457                 |
rel error ex :    | 0.926322448191736928E-15   |
rel error ey :    | 0.857027199757830407E-15   |
rel error ez :    | 0.909135877931033964E-15   |
rel error pot:    | 0.327680288122564076E-15   |

================= | testing multipole interaction | ===============
----------------- | ----------------------------- | ---------------
Total time:       | 2.71204633                    |
Reference time:   | 7.89788709                    |
rel error ex :    | 0.741593034088542904E-13      |
rel error ey :    | 0.155725877788571450E-12      |
rel error ez :    | 0.103623005375644965E-11      |
rel error pot:    | 0.112714781613199641E-13      |

Run times have changed for reasons unknown.

### Exploit additional opportunity for `MSUB`

An additional `MSUB` can be used in the multipole interaction.

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -40,6 +40,8 @@ module bla

         ! a * b * c
         #define VEC_MUL3(a,b,c) VEC_MUL(a, VEC_MUL(b,c))
    +    ! a * b * c * d
    +    #define VEC_MUL4(a,b,c,d) VEC_MUL(a, VEC_MUL3(b,c,d))
         ! a * b + c * d
         #define VEC_aTbMcTd(a,b,c,d) VEC_MSUB(a,b,VEC_MUL(c,d))
         ! a * b + c * d + e * f
    @@ -167,12 +169,12 @@ module bla
             dy2 = VEC_MUL(dy, dy)
             dz2 = VEC_MUL(dz, dz)

    -        fd1 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dx2), rd5), rd3)
    -        fd2 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dy2), rd5), rd3)
    -        fd3 = VEC_SUB(VEC_MUL(VEC_MUL(vthree, dz2), rd5), rd3)
    -        fd4 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dx), dy), rd5)
    -        fd5 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dy), dz), rd5)
    -        fd6 = VEC_MUL(VEC_MUL(VEC_MUL(vthree, dx), dz), rd5)
    +        fd1 = VEC_MUL3SUB(vthree, dx2, rd5, rd3)
    +        fd2 = VEC_MUL3SUB(vthree, dy2, rd5, rd3)
    +        fd3 = VEC_MUL3SUB(vthree, dz2, rd5, rd3)
    +        fd4 = VEC_MUL4(vthree, dx, dy, rd5)
    +        fd5 = VEC_MUL4(vthree, dy, dz, rd5)
    +        fd6 = VEC_MUL4(vthree, dx, dz, rd5)

             vfive_rd7    = VEC_MUL(vfive, rd7)
             vfive_rd7_dx = VEC_MUL(vfive_rd7, dx)

================= | testing direct interaction | ===============
----------------- | -------------------------- | ---------------
Total time:       | 0.61913399                 |
Reference time:   | 2.51986145                 |
rel error ex :    | 0.926322448191736928E-15   |
rel error ey :    | 0.857027199757830407E-15   |
rel error ez :    | 0.909135877931033964E-15   |
rel error pot:    | 0.327680288122564076E-15   |

================= | testing multipole interaction | ===============
----------------- | ----------------------------- | ---------------
Total time:       | 3.21384209                    |
Reference time:   | 7.89150987                    |
rel error ex :    | 0.741593034088542904E-13      |
rel error ey :    | 0.155725877788571450E-12      |
rel error ez :    | 0.103623005375644965E-11      |
rel error pot:    | 0.644582430468651833E-13      |

Run times are slightly up. Wait for it...

### Reordering operands

A slight reordering of operands brings performance back up.

    :::diff
    --- a/main.f90
    +++ b/main.f90
    @@ -39,17 +39,17 @@ module bla
       contains

         ! a * b * c
    -    #define VEC_MUL3(a,b,c) VEC_MUL(a, VEC_MUL(b,c))
    +    #define VEC_MUL3(a,b,c) VEC_MUL(VEC_MUL(a,b), c)
         ! a * b * c * d
    -    #define VEC_MUL4(a,b,c,d) VEC_MUL(a, VEC_MUL3(b,c,d))
    +    #define VEC_MUL4(a,b,c,d) VEC_MUL(VEC_MUL3(a,b,c), d)
         ! a * b + c * d
         #define VEC_aTbMcTd(a,b,c,d) VEC_MSUB(a,b,VEC_MUL(c,d))
         ! a * b + c * d + e * f
    -    #define VEC_aTbPcTdPeTf(a,b,c,d,e,f) VEC_MADD(a,b,VEC_MADD(c,d,VEC_MUL(e,f)))
    +    #define VEC_aTbPcTdPeTf(a,b,c,d,e,f) VEC_MADD(e,f,VEC_MADD(c,d,VEC_MUL(a,b)))
         ! a * b * c -d
    -    #define VEC_MUL3SUB(a,b,c,d) VEC_MSUB(a, VEC_MUL(b,c), d)
    +    #define VEC_MUL3SUB(a,b,c,d) VEC_MSUB(VEC_MUL(a,b), c, d)
         ! performs variable(index) += summand
    -    #define VEC_ACCUMULATE(index, variable, summand) VEC_STA(VEC_ADD(VEC_LDA(index,variable),summand),index,variable)
    +    #define VEC_ACCUMULATE(index, variable, summand) VEC_STA(VEC_ADD(summand,VEC_LDA(index,variable)),index,variable)
         ! performs variable(index) += a * b
         #define VEC_MACCUMULATE(index, variable, a, b) VEC_STA(VEC_MADD(a,b,VEC_LDA(index,variable)),index,variable)

================= | testing direct interaction | ===============
----------------- | -------------------------- | ---------------
Total time:       | 0.61753965                 |
Reference time:   | 2.51210555                 |
rel error ex :    | 0.926322448191736928E-15   |
rel error ey :    | 0.912494413696586186E-15   |
rel error ez :    | 0.909135877931033964E-15   |
rel error pot:    | 0.327680288122564076E-15   |

================= | testing multipole interaction | ===============
----------------- | ----------------------------- | ---------------
Total time:       | 2.67512282                    |
Reference time:   | 7.90619606                    |
rel error ex :    | 0.714126625418596913E-13      |
rel error ey :    | 0.172675633194130262E-12      |
rel error ez :    | 0.135507007029689564E-11      |
rel error pot:    | 0.383462659096452403E-14      |

