
module bla
  implicit none
  save
    integer, parameter :: NN = 512
    integer, parameter :: Niter = 100000
!
    integer, parameter :: kfp = 8 ! numeric precision (kind value)
    integer, public, parameter :: kind_particle     = kind(1_8) ! ATTENTION, see above
    real(kfp), parameter :: zero    =  0._kfp
    real(kfp), parameter :: three   =  3._kfp
    real(kfp), parameter :: one     =  1._kfp
    real(kfp), parameter :: five    =  5._kfp
    real(kfp), parameter :: half    =  0.5_kfp
    real(kfp) :: eps = 0.1

      type t_particle_pack
         real*8, allocatable :: ex(:)
         real*8, allocatable :: ey(:)
         real*8, allocatable :: ez(:)
         real*8, allocatable :: pot(:)
      end type t_particle_pack

      type t_tree_node_interaction_data
        real*8 :: charge ! net charge sum
        real*8 :: dip(3) ! dipole moment
        real*8 :: quad(3) ! diagonal quadrupole moments
        real*8 :: xyquad ! other quadrupole moments
        real*8 :: yzquad
        real*8 :: zxquad
      end type t_tree_node_interaction_data


    public calc_force_coulomb_3D_direct
    public calc_force_coulomb_3D_direct_reference
    public clear_results
    public rel_err_norm

  contains

    ! a * b * c
    #define VEC_MUL3(a,b,c) VEC_MUL(VEC_MUL(a,b), c)
    ! a * b * c * d
    #define VEC_MUL4(a,b,c,d) VEC_MUL(VEC_MUL3(a,b,c), d)
    ! a * b + c * d
    #define VEC_aTbMcTd(a,b,c,d) VEC_MSUB(a,b,VEC_MUL(c,d))
    ! a * b + c * d + e * f
    #define VEC_aTbPcTdPeTf(a,b,c,d,e,f) VEC_MADD(e,f,VEC_MADD(c,d,VEC_MUL(a,b)))
    ! a * b * c -d
    #define VEC_MUL3SUB(a,b,c,d) VEC_MSUB(VEC_MUL(a,b), c, d)
    ! performs variable(index) += summand
    #define VEC_ACCUMULATE(index, variable, summand) VEC_STA(VEC_ADD(summand,VEC_LDA(index,variable)),index,variable)
    ! performs variable(index) += a * b
    #define VEC_MACCUMULATE(index, variable, a, b) VEC_STA(VEC_MADD(a,b,VEC_LDA(index,variable)),index,variable)


    subroutine calc_force_coulomb_3D_direct(delta, dist2, particle_pack, t, eps2)
      implicit none

      real(kfp), intent(in) :: delta(:,:)
      real(kfp), intent(in) :: dist2(:)
      type(t_particle_pack), intent(inout) :: particle_pack
      type(t_tree_node_interaction_data), intent(in) :: t
      real(kfp), intent(in) :: eps2

      VECTOR(REAL(kfp)) :: rd,r,rd3tcharge,rdtcharge, vzero, vdist2, vone, vtcharge, veps2
      integer(kind_particle) :: ip, np

      vzero    = VEC_SPLATS(zero)
      vone     = VEC_SPLATS(one)
      vtcharge = VEC_SPLATS(t%charge)
      veps2    = VEC_SPLATS(eps2)

      np = 8*size(dist2, kind = kind_particle)

      !ibm* assert(nodeps)
      do ip=0,np-8,32
          vdist2 = VEC_LDA(ip, dist2)

          ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
          r  = VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) )
          rd = VEC_SEL(vzero, VEC_SWDIV_NOCHK(vone, r), VEC_CMPGT(vdist2, vzero))

          rdtcharge  = VEC_MUL(vtcharge, rd)
          call VEC_ACCUMULATE(ip, particle_pack%pot, rdtcharge)
          rd3tcharge = VEC_MUL3(rdtcharge,rd,rd)
          call VEC_MACCUMULATE(ip, particle_pack%ex, rd3tcharge, VEC_LDA(      ip, delta))
          call VEC_MACCUMULATE(ip, particle_pack%ey, rd3tcharge, VEC_LDA(   np+ip, delta))
          call VEC_MACCUMULATE(ip, particle_pack%ez, rd3tcharge, VEC_LDA(np+np+ip, delta))
      end do

    end subroutine calc_force_coulomb_3D_direct

    subroutine calc_force_coulomb_3D_direct_reference(delta, dist2, particle_pack, t, eps2)
      implicit none

      real(kfp), intent(in) :: delta(:,:)
      real(kfp), intent(in) :: dist2(:)
      type(t_particle_pack), intent(inout) :: particle_pack
      type(t_tree_node_interaction_data), intent(in) :: t
      real(kfp), intent(in) :: eps2

      real(kfp) :: rd,r,rd3charge
      integer(kind_particle) :: ip, np

      np = size(dist2, kind = kind_particle)

      do ip = 1, np

        if (dist2(ip) > zero) then
          r = sqrt(dist2(ip) + eps2)
          rd = one/r
          rd3charge = t%charge*rd*rd*rd

          particle_pack%pot(ip) = particle_pack%pot(ip) + t%charge*rd
          particle_pack%ex(ip) = particle_pack%ex(ip) + rd3charge*delta(ip,1)
          particle_pack%ey(ip) = particle_pack%ey(ip) + rd3charge*delta(ip,2)
          particle_pack%ez(ip) = particle_pack%ez(ip) + rd3charge*delta(ip,3)
        end if
      end do
    end subroutine calc_force_coulomb_3D_direct_reference

    subroutine calc_force_coulomb_3D(delta, dist2, particle_pack, t, eps2)
      implicit none

      real(kfp), intent(in) :: delta(:,:)
      real(kfp), intent(in) :: dist2(:)
      type(t_particle_pack), intent(inout) :: particle_pack
      type(t_tree_node_interaction_data), intent(in) :: t
      real(kfp), intent(in) :: eps2

      vector(real(kfp)) :: rd,dx,dy,dz,dx2,dy2,dz2,rd2,rd3,rd5,rd7,fd1,fd2,fd3,fd4,fd5,fd6
      vector(real(kfp)) :: veps2, vone, vthree, vfive, vhalf, vdist2, vtcharge, vtdip(3), vtquad(3), vtxyquad, vtzxquad, vtyzquad, &
                           vfive_rd7, dx_rd5, dy_rd5, dz_rd5, vtcharge_rd3, vfive_rd7_dx, vfive_rd7_dy, vfive_rd7_dz,vfive_rd7_dxdydz
      integer(kind_particle) :: ip, np

      veps2    = VEC_SPLATS(eps2)
      vone     = VEC_SPLATS(one)
      vthree   = VEC_SPLATS(three)
      vfive    = VEC_SPLATS(five)
      vhalf    = VEC_SPLATS(half)

      vtcharge = VEC_SPLATS(t%charge)
      do ip=1,3
        vtdip(ip)  = VEC_SPLATS(t%dip(ip))
        vtquad(ip) = VEC_SPLATS(t%quad(ip))
      end do
      vtxyquad = VEC_SPLATS(t%xyquad)
      vtyzquad = VEC_SPLATS(t%yzquad)
      vtzxquad = VEC_SPLATS(t%zxquad)

      np = 8*size(dist2, kind = kind_particle)

      !ibm* assert(nodeps)
      do ip=0,np-8,32
        ! see http://pic.dhe.ibm.com/infocenter/compbg/v121v141/topic/com.ibm.xlf141.bg.doc/language_ref/vmxintrinsics.html
        vdist2 = VEC_LDA(ip, dist2)

        rd  = VEC_SWDIV_NOCHK(vone, VEC_SWSQRT_NOCHK( VEC_ADD(vdist2, veps2) ))
        rd2 = VEC_MUL(rd,  rd )
        rd3 = VEC_MUL(rd,  rd2)
        rd5 = VEC_MUL(rd3, rd2)
        rd7 = VEC_MUL(rd5, rd2)

        dx = VEC_LDA(      ip, delta)
        dy = VEC_LDA(   np+ip, delta)
        dz = VEC_LDA(np+np+ip, delta)
        dx2 = VEC_MUL(dx, dx)
        dy2 = VEC_MUL(dy, dy)
        dz2 = VEC_MUL(dz, dz)

        fd1 = VEC_MUL3SUB(vthree, dx2, rd5, rd3)
        fd2 = VEC_MUL3SUB(vthree, dy2, rd5, rd3)
        fd3 = VEC_MUL3SUB(vthree, dz2, rd5, rd3)
        fd4 = VEC_MUL4(vthree, dx, dy, rd5)
        fd5 = VEC_MUL4(vthree, dy, dz, rd5)
        fd6 = VEC_MUL4(vthree, dx, dz, rd5)

        vfive_rd7    = VEC_MUL(vfive, rd7)
        vfive_rd7_dx = VEC_MUL(vfive_rd7, dx)
        vfive_rd7_dy = VEC_MUL(vfive_rd7, dy)
        vfive_rd7_dz = VEC_MUL(vfive_rd7, dz)
        vfive_rd7_dxdydz = VEC_MUL3(vfive_rd7_dx, dy, dz)
        dx_rd5       = VEC_MUL(dx, rd5)
        dy_rd5       = VEC_MUL(dy, rd5)
        dz_rd5       = VEC_MUL(dz, rd5)
        vtcharge_rd3 = VEC_MUL(vtcharge, rd3)

        call VEC_ACCUMULATE(ip, particle_pack%pot,
                VEC_MADD(vtcharge,
                         rd,
                         VEC_MADD(rd3,
                                  VEC_aTbPcTdPeTf(dx,vtdip(1),dy,vtdip(2),dz,vtdip(3)),
                                  VEC_MADD(vhalf,
                                           VEC_aTbPcTdPeTf(fd1,vtquad(1),fd2,vtquad(2),fd3,vtquad(3)),
                                           VEC_aTbPcTdPeTf(fd4,vtxyquad,fd5,vtyzquad,fd6,vtzxquad)
                                           )
                                  )
                         )
                        )

        call VEC_ACCUMULATE(ip,particle_pack%ex,
                  VEC_MADD( vthree,
                            VEC_MADD( vhalf,
                                      VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dx,dx2,dx_rd5,vthree),vtquad(1),
                                                      VEC_MSUB(   vfive_rd7_dx,dy2,dx_rd5)       ,vtquad(2),
                                                      VEC_MSUB(   vfive_rd7_dx,dz2,dx_rd5)       ,vtquad(3)
                                                      ),
                                      VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dy,dx2,dy_rd5), vtxyquad,
                                                      VEC_MSUB(vfive_rd7_dz,dx2,dz_rd5), vtzxquad,
                                                      vfive_rd7_dxdydz,                  vtyzquad
                                                      )
                                      ),
                            VEC_MADD( vtcharge_rd3,
                                      dx,
                                      VEC_aTbPcTdPeTf(fd1,vtdip(1),
                                                      fd4,vtdip(2),
                                                      fd6,vtdip(3))
                                     )
                           )
                          )

        call VEC_ACCUMULATE(ip,particle_pack%ey,
                  VEC_MADD( vthree,
                            VEC_MADD( vhalf,
                                      VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dy,dy2,dy_rd5,vthree),vtquad(2),
                                                      VEC_MSUB(   vfive_rd7_dy,dx2,dy_rd5)       ,vtquad(1),
                                                      VEC_MSUB(   vfive_rd7_dy,dz2,dy_rd5)       ,vtquad(3)
                                                      ),
                                      VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dx,dy2,dx_rd5 ),vtxyquad,
                                                      VEC_MSUB(vfive_rd7_dz,dy2,dz_rd5 ),vtyzquad,
                                                      vfive_rd7_dxdydz,                  vtzxquad
                                                      )
                                      ),
                            VEC_MADD(vtcharge_rd3,
                                     dy,
                                     VEC_aTbPcTdPeTf(fd2,vtdip(2),
                                                     fd4,vtdip(1),
                                                     fd5,vtdip(3))
                                     )
                          )
                         )

        call VEC_ACCUMULATE(ip,particle_pack%ez,
                  VEC_MADD( vthree,
                            VEC_MADD( vhalf,
                                      VEC_aTbPcTdPeTf(VEC_aTbMcTd(vfive_rd7_dz,dz2,dz_rd5,vthree),vtquad(3),
                                                      VEC_MSUB(   vfive_rd7_dz,dy2,dz_rd5)       ,vtquad(2),
                                                      VEC_MSUB(   vfive_rd7_dz,dx2,dz_rd5)       ,vtquad(1)
                                                      ),
                                      VEC_aTbPcTdPeTf(VEC_MSUB(vfive_rd7_dx,dz2,dx_rd5 ),vtzxquad,
                                                      VEC_MSUB(vfive_rd7_dy,dz2,dy_rd5 ),vtyzquad,
                                                      vfive_rd7_dxdydz,                  vtxyquad
                                                      )
                                      ),
                            VEC_MADD(vtcharge_rd3,
                                     dz,
                                     VEC_aTbPcTdPeTf(fd3,vtdip(3),
                                                     fd5,vtdip(2),
                                                     fd6,vtdip(1))
                                     )
                          )
                         )
      end do
    end subroutine calc_force_coulomb_3D

    subroutine calc_force_coulomb_3D_reference(delta, dist2, particle_pack, t, eps2)
      implicit none

      real(kfp), intent(in) :: delta(:,:)
      real(kfp), intent(in) :: dist2(:)
      type(t_particle_pack), intent(inout) :: particle_pack
      type(t_tree_node_interaction_data), intent(in) :: t
      real(kfp), intent(in) :: eps2

      real(kfp) :: rd,dx,dy,dz,r,dx2,dy2,dz2,dx3,dy3,dz3,rd2,rd3,rd5,rd7,fd1,fd2,fd3,fd4,fd5,fd6
      integer(kind_particle) :: ip, np

      np = size(dist2, kind = kind_particle)

      do ip = 1, np
        dx = delta(ip, 1)
        dy = delta(ip, 2)
        dz = delta(ip, 3)

        r  = sqrt(dist2(ip) + eps2)
        rd = one/r
        rd2 = rd *rd
        rd3 = rd *rd2
        rd5 = rd3*rd2
        rd7 = rd5*rd2

        dx2 = dx*dx
        dy2 = dy*dy
        dz2 = dz*dz
        dx3 = dx*dx2
        dy3 = dy*dy2
        dz3 = dz*dz2

        fd1 = three*dx2*rd5 - rd3
        fd2 = three*dy2*rd5 - rd3
        fd3 = three*dz2*rd5 - rd3
        fd4 = three*dx*dy*rd5
        fd5 = three*dy*dz*rd5
        fd6 = three*dx*dz*rd5

        particle_pack%pot(ip) = particle_pack%pot(ip) &
              + t%charge*rd                                           &  !  monopole term
              + (dx*t%dip(1) + dy*t%dip(2) + dz*t%dip(3))*rd3         &  !  dipole
              + half*(fd1*t%quad(1) + fd2*t%quad(2) + fd3*t%quad(3))  &  !  quadrupole
              +       fd4*t%xyquad  + fd5*t%yzquad  + fd6*t%zxquad

        particle_pack%ex(ip) = particle_pack%ex(ip) &
                  + t%charge*dx*rd3                                  &  ! monopole term
                  + fd1*t%dip(1) + fd4*t%dip(2) + fd6*t%dip(3)       &  ! dipole term
                  + three * (                                        &  ! quadrupole term
                    half * (                                         &
                        ( five*dx3   *rd7 - three*dx*rd5 )*t%quad(1) &
                      + ( five*dx*dy2*rd7 -       dx*rd5 )*t%quad(2) &
                      + ( five*dx*dz2*rd7 -       dx*rd5 )*t%quad(3) &
                    )                                                &
                    + ( five*dy*dx2  *rd7 - dy*rd5 )*t%xyquad        &
                    + ( five*dz*dx2  *rd7 - dz*rd5 )*t%zxquad        &
                    + ( five*dx*dy*dz*rd7          )*t%yzquad        &
                    )

        particle_pack%ey(ip) = particle_pack%ey(ip) &
                  + t%charge*dy*rd3                                  &
                  + fd2*t%dip(2) + fd4*t%dip(1) + fd5*t%dip(3)       &
                  + three * (                                        &
                    half * (                                         &
                        ( five*dy3*rd7    - three*dy*rd5 )*t%quad(2) &
                      + ( five*dy*dx2*rd7 -       dy*rd5 )*t%quad(1) &
                      + ( five*dy*dz2*rd7 -       dy*rd5 )*t%quad(3) &
                    )                                                &
                    + ( five*dx*dy2  *rd7 - dx*rd5 )*t%xyquad        &
                    + ( five*dz*dy2  *rd7 - dz*rd5 )*t%yzquad        &
                    + ( five*dx*dy*dz*rd7          )*t%zxquad        &
                    )

        particle_pack%ez(ip) = particle_pack%ez(ip) &
                  + t%charge*dz*rd3                                  &
                  + fd3*t%dip(3) + fd5*t%dip(2) + fd6*t%dip(1)       &
                  + three * (                                        &
                    half * (                                         &
                      + ( five*dz3   *rd7 - three*dz*rd5 )*t%quad(3) &
                      + ( five*dz*dy2*rd7 -       dz*rd5 )*t%quad(2) &
                      + ( five*dz*dx2*rd7 -       dz*rd5 )*t%quad(1) &
                    )                                                &
                    + ( five*dx*dz2  *rd7 - dx*rd5 )*t%zxquad        &
                    + ( five*dy*dz2  *rd7 - dy*rd5 )*t%yzquad        &
                    + ( five*dx*dy*dz*rd7          )*t%xyquad        &
                    )
      end do
    end subroutine calc_force_coulomb_3D_reference

    subroutine clear_results(p)
      implicit none
      type(t_particle_pack), intent(inout) :: p

      p%ex  = zero
      p%ey  = zero
      p%ez  = zero
      p%pot = zero

    end subroutine

    real(kfp) function rel_err_norm(f1, f2)
      implicit none
      real(kfp), intent(in) :: f1(:), f2(:)
      rel_err_norm = maxval(abs((f1-f2)/f2))
    end function

end module bla


program main
  use bla
  implicit none
  include 'mpif.h'

  type(t_particle_pack) :: p, p_ref
  real(kfp), allocatable :: delta(:,:)
  real(kfp), allocatable :: dist2(:)
  type(t_tree_node_interaction_data) :: node
  real*8 :: t1, t2
  integer :: i


  allocate(p%ex(NN), p%ey(NN), p%ez(NN), p%pot(NN))
  allocate(p_ref%ex(NN), p_ref%ey(NN), p_ref%ez(NN), p_ref%pot(NN))
  allocate(dist2(NN), delta(NN,1:3))

  call random_number(dist2)
  call random_number(delta)
  call random_number(node%charge)
  call random_number(node%dip)
  call random_number(node%quad)
  call random_number(node%xyquad)
  call random_number(node%yzquad)
  call random_number(node%zxquad)

  write(*,*) '================= testing direct interaction ==============='
  call clear_results(p)
  call clear_results(p_ref)
  t1 = MPI_WTIME()
  do i=1,Niter
    call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
  end do
  t2 = MPI_WTIME()
  write(*,'("Total time:     ", f12.8)') t2-t1

  t1 = MPI_WTIME()
  do i=1,Niter
    call calc_force_coulomb_3D_direct_reference(delta, dist2, p_ref, node, eps)
  end do
  t2 = MPI_WTIME()
  write(*,'("Reference time: ", f12.8)') t2-t1

  call clear_results(p)
  call clear_results(p_ref)
  call calc_force_coulomb_3D_direct(delta, dist2, p, node, eps)
  call calc_force_coulomb_3D_direct_reference(delta, dist2, p_ref, node, eps)

  write(*,*) 'rel error ex :', rel_err_norm(p%ex, p_ref%ex)
  write(*,*) 'rel error ey :', rel_err_norm(p%ey, p_ref%ey)
  write(*,*) 'rel error ez :', rel_err_norm(p%ez, p_ref%ez)
  write(*,*) 'rel error pot:', rel_err_norm(p%pot, p_ref%pot)

  write(*,*) '================= testing multipole interaction ==============='
  call clear_results(p)
  call clear_results(p_ref)
  t1 = MPI_WTIME()
  do i=1,Niter
    call calc_force_coulomb_3D(delta, dist2, p, node, eps)
  end do
  t2 = MPI_WTIME()
  write(*,'("Total time:     ", f12.8)') t2-t1

  t1 = MPI_WTIME()
  do i=1,Niter
    call calc_force_coulomb_3D_reference(delta, dist2, p_ref, node, eps)
  end do
  t2 = MPI_WTIME()
  write(*,'("Reference time: ", f12.8)') t2-t1

  call clear_results(p)
  call clear_results(p_ref)
  call calc_force_coulomb_3D(delta, dist2, p, node, eps)
  call calc_force_coulomb_3D_reference(delta, dist2, p_ref, node, eps)

  write(*,*) 'rel error ex :', rel_err_norm(p%ex, p_ref%ex)
  write(*,*) 'rel error ey :', rel_err_norm(p%ey, p_ref%ey)
  write(*,*) 'rel error ez :', rel_err_norm(p%ez, p_ref%ez)
  write(*,*) 'rel error pot:', rel_err_norm(p%pot, p_ref%pot)

  deallocate(p%ex, p%ey, p%ez, p%pot)
  deallocate(p_ref%ex, p_ref%ey, p_ref%ez, p_ref%pot)
  deallocate(dist2, delta)

end program
